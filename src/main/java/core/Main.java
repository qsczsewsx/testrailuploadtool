package core;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import core.testrail.entity.Case;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

@SuppressWarnings("rawtypes")
public final class Main extends Application {

	public static LoggerApp logger = LoggerApp.getInstance();

	@Override
	public void start(final Stage stage) {

		LogView logView = new LogView(logger);
		logView.setPrefWidth(400);
		stage.setTitle("Upload testrail from excel file tool");

		final FileChooser fileChooser = new FileChooser();

		final Button openButton = new Button("Select excel file");

		openButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				File file = fileChooser.showOpenDialog(stage);
				if (file != null) {
					try {
						Thread generatorThread = new Thread(
			                    () -> {
			                    	try {
			                    		logger.info("File name is: "+file.getPath());
										openFile(file);
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										logger.error(e1.getMessage());
										e1.printStackTrace();
									}
			                    },
			                    "OpenFile"
			            );
			            generatorThread.setDaemon(true);
			            generatorThread.start();
						
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						// ConfirmBox.display("Alert result", e1.getMessage());
						logger.error(e1.getMessage());
						e1.printStackTrace();
					}
				}
			}
		});

		final GridPane inputGridPane = new GridPane();

		GridPane.setConstraints(openButton, 0, 0);
		inputGridPane.setHgap(6);
		inputGridPane.setVgap(6);
		inputGridPane.getChildren().addAll(openButton);
		inputGridPane.setAlignment(Pos.BASELINE_CENTER);
				
		ChoiceBox<Level> filterLevel = new ChoiceBox<>(
                FXCollections.observableArrayList(
                        Level.values()
                )
        );
        filterLevel.getSelectionModel().select(Level.DEBUG);
        logView.filterLevelProperty().bind(
                filterLevel.getSelectionModel().selectedItemProperty()
        );
		Slider rate = new Slider(1, 60, 60);
        logView.refreshRateProperty().bind(rate.valueProperty());
        Label rateLabel = new Label();
        rateLabel.textProperty().bind(Bindings.format("Update: %.2f fps", rate.valueProperty()));
        rateLabel.setStyle("-fx-font-family: monospace;");
        VBox rateLayout = new VBox(rate, rateLabel);
        rateLayout.setAlignment(Pos.CENTER);
		VBox layout = new VBox(10, logView);
		VBox.setVgrow(logView, Priority.ALWAYS);

		final Pane rootGroup = new VBox(12);
		rootGroup.getChildren().addAll(inputGridPane, layout);
		rootGroup.setPadding(new Insets(12, 12, 12, 12));

		Scene scene = new Scene(rootGroup, 300, 250);
		scene.getStylesheets().add(this.getClass().getResource("log-view.css").toExternalForm());
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}

	private void openFile(File file) throws Exception {
		logger.info("start application");
		AddTestCase atc = new AddTestCase();
		List<HashMap> caseMapList = atc.getCasesFromCsv(file.getPath());
		atc.addCasesFromCsv(caseMapList);
		logger.alert("------------------SUCCESSFUL ALL---------------------------");
	}

}