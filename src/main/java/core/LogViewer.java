package core;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.css.PseudoClass;
import javafx.scene.control.*;
import javafx.util.Duration;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

class Log {
	private static final int MAX_LOG_ENTRIES = 1_000_000;

	private final BlockingDeque<LogRecord> log = new LinkedBlockingDeque<>(MAX_LOG_ENTRIES);

	public void drainTo(Collection<? super LogRecord> collection) {
		log.drainTo(collection);
	}

	public void offer(LogRecord record) {
		log.offer(record);
	}
}

class LoggerApp {
	private final Log log;
	private final String context;
	private static LoggerApp logger = null;

	public LoggerApp(Log log, String context) {
		this.log = log;
		this.context = context;
	}
	
	public static LoggerApp getInstance() {
		if(logger == null)
			logger = new LoggerApp(new Log(),"");
		return logger;
	}

	public void log(LogRecord record) {
		log.offer(record);
	}

	public void debug(String msg) {
		log(new LogRecord(Level.DEBUG, context, msg));
	}

	public void info(String msg) {
		log(new LogRecord(Level.INFO, context, msg));
	}

	public void warn(String msg) {
		log(new LogRecord(Level.WARN, context, msg));
	}

	public void error(String msg) {
		log(new LogRecord(Level.ERROR, context, msg));
	}
	
	public void alert(String msg) {
		log(new LogRecord(Level.ALERT, context, msg));
	}
	public Log getLog() {
		return log;
	}
}

enum Level {
	DEBUG, INFO, WARN, ERROR, ALERT
}

class LogRecord {
	private Date timestamp;
	private Level level;
	private String context;
	private String message;

	public LogRecord(Level level, String context, String message) {
		this.timestamp = new Date();
		this.level = level;
		this.context = context;
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public Level getLevel() {
		return level;
	}

	public String getContext() {
		return context;
	}

	public String getMessage() {
		return message;
	}
}

class LogView extends ListView<LogRecord> {
	private static final int MAX_ENTRIES = 10_000;

	private final static PseudoClass debug = PseudoClass.getPseudoClass("debug");
	private final static PseudoClass info = PseudoClass.getPseudoClass("info");
	private final static PseudoClass warn = PseudoClass.getPseudoClass("warn");
	private final static PseudoClass error = PseudoClass.getPseudoClass("error");
	private final static PseudoClass alert = PseudoClass.getPseudoClass("alert");

	private final static SimpleDateFormat timestampFormatter = new SimpleDateFormat("HH:mm:ss.SSS");

	private final BooleanProperty showTimestamp = new SimpleBooleanProperty(false);
	private final ObjectProperty<Level> filterLevel = new SimpleObjectProperty<>(null);
	private final BooleanProperty tail = new SimpleBooleanProperty(false);
	private final BooleanProperty paused = new SimpleBooleanProperty(false);
	private final DoubleProperty refreshRate = new SimpleDoubleProperty(0.5);

	private final ObservableList<LogRecord> logItems = FXCollections.observableArrayList();

	public BooleanProperty showTimeStampProperty() {
		return showTimestamp;
	}

	public ObjectProperty<Level> filterLevelProperty() {
		return filterLevel;
	}

	public BooleanProperty tailProperty() {
		return tail;
	}

	public BooleanProperty pausedProperty() {
		return paused;
	}

	public DoubleProperty refreshRateProperty() {
		return refreshRate;
	}

	public LogView(LoggerApp logger) {
		getStyleClass().add("log-view");

		Timeline logTransfer = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
			logger.getLog().drainTo(logItems);

			if (logItems.size() > MAX_ENTRIES) {
				logItems.remove(0, logItems.size() - MAX_ENTRIES);
			}

			if (tail.get()) {
				scrollTo(logItems.size());
			}
		}));
		logTransfer.setCycleCount(Timeline.INDEFINITE);
		logTransfer.rateProperty().bind(refreshRateProperty());

		this.pausedProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue && logTransfer.getStatus() == Animation.Status.RUNNING) {
				logTransfer.pause();
			}

			if (!newValue && logTransfer.getStatus() == Animation.Status.PAUSED && getParent() != null) {
				logTransfer.play();
			}
		});

		this.parentProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue == null) {
				logTransfer.pause();
			} else {
				if (!paused.get()) {
					logTransfer.play();
				}
			}
		});

		filterLevel.addListener((observable, oldValue, newValue) -> {
			setItems(new FilteredList<LogRecord>(logItems,
					logRecord -> logRecord.getLevel().ordinal() >= filterLevel.get().ordinal()));
		});
		filterLevel.set(Level.DEBUG);

		setCellFactory(param -> new ListCell<LogRecord>() {
			{
				showTimestamp.addListener(observable -> updateItem(this.getItem(), this.isEmpty()));
			}

			@Override
			protected void updateItem(LogRecord item, boolean empty) {
				super.updateItem(item, empty);

				pseudoClassStateChanged(debug, false);
				pseudoClassStateChanged(info, false);
				pseudoClassStateChanged(warn, false);
				pseudoClassStateChanged(error, false);
				pseudoClassStateChanged(alert, false);

				if (item == null || empty) {
					setText(null);
					return;
				}

				String context = (item.getContext() == null) ? "" : item.getContext() + " ";

				if (showTimestamp.get()) {
					String timestamp = (item.getTimestamp() == null) ? ""
							: timestampFormatter.format(item.getTimestamp()) + " ";
					setText(timestamp + context + item.getMessage());
				} else {
					setText(context + item.getMessage());
				}

				switch (item.getLevel()) {
				case DEBUG:
					pseudoClassStateChanged(debug, true);
					break;

				case INFO:
					pseudoClassStateChanged(info, true);
					break;

				case WARN:
					pseudoClassStateChanged(warn, true);
					break;

				case ERROR:
					pseudoClassStateChanged(error, true);
					break;
					
				case ALERT:
					pseudoClassStateChanged(alert, true);
					break;
				}
			}
		});
	}
}
