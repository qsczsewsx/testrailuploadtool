package core;

import com.google.gson.annotations.SerializedName;

import core.testrail.entity.ITestRail;

public class Section implements ITestRail {

	private Integer id;
	/**
	 * The ID of the user the entire test run is assigned to
	 */
	@SerializedName("assignedto_id")
	private Integer assignedtoId;
	@SerializedName("suite_id")
	private Integer suiteId;
	@SerializedName("parent_id")
	private Integer parentId;
	
	private String name;
	
	@SerializedName("display_order")
	private Integer displayOrder;
	private String description;
	private Integer depth;
	  
	  
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAssignedtoId() {
		return assignedtoId;
	}

	public void setAssignedtoId(Integer assignedtoId) {
		this.assignedtoId = assignedtoId;
	}

	public Integer getSuiteId() {
		return suiteId;
	}

	public void setSuiteId(Integer suiteId) {
		this.suiteId = suiteId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDepth() {
		return depth;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	@Override
	  public Class<?> getClazz() {
	    return Section.class;
	  }

	  @Override
	  public Class<?> getClazzArray() {
	    return Section[].class;
	  }

	  @Override
	  public String getTestRailURL() {
	    return "get_section";
	  }

	  @Override
	  public String getListTestRailURL() {
	    return "get_sections";
	  }

	  @Override
	  public String addTestRailURL() {
	    return "add_section";
	  }

	  @Override
	  public String updateTestRailURL() {
	    return "update_section";
	  }

	  @Override
	  public String deleteTestRailURL() {
	    return "delete_section";
	  }
}
