package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.google.gson.Gson;

import core.common.UtilsClass;
import core.tdm.ExcelUtil;
import core.testrail.TestRailUtils;
import core.testrail.entity.Case;
import core.testrail.entity.CustomStepsSeparated;
import core.testrail.entity.Result;
import core.testrail.entity.Run;
import core.testrail.entity.Test;

public class AddTestCase {

	public static LoggerApp logger = LoggerApp.getInstance();
	private ExcelUtil excel;

	private HashMap<String, Integer> loadCsvHeader(String fileName)
			throws FileNotFoundException, IOException, Exception {

		FileInputStream ExcelFile = new FileInputStream(fileName);
		XSSFWorkbook excelWBook = new XSSFWorkbook(ExcelFile);
		XSSFSheet excelWSheet = excelWBook.getSheetAt(0);

		Iterator<Row> rowIterator = excelWSheet.iterator();
		Row row = rowIterator.next();
		Iterator<Cell> cellIterator = row.cellIterator();
		HashMap<String, Integer> header = new HashMap<>();

		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
			header.put(cell.getStringCellValue(), cell.getColumnIndex());
		}
		excelWBook.close();
		return header;
	}

	private Object[][] loadCsv(String fileName) throws FileNotFoundException, IOException, Exception {
		FileInputStream ExcelFile = new FileInputStream(fileName);
		XSSFWorkbook excelWBook = new XSSFWorkbook(ExcelFile);
		XSSFSheet excelWSheet = excelWBook.getSheetAt(0);
		String sheetName = excelWSheet.getSheetName();
		excelWBook.close();
		excel = new ExcelUtil();
		excel.setExcelFile(fileName, sheetName);
		return excel.getTableArray(fileName, sheetName, null);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<HashMap> getCasesFromCsv(String fileName) throws Exception {
		Object[][] data = loadCsv(fileName);
		HashMap<String, Integer> header = loadCsvHeader(fileName);
		List<HashMap> caseMapList = new ArrayList<>();
		HashMap caseMap = null;
		HashMap step;
		List<HashMap> steps = null;
		int jTitle;
		if (UtilsClass.isNOTNullEmpty(header) && header.get(TestCaseConstants.CSV_TITLE) >= 0) {
			jTitle = header.get(TestCaseConstants.CSV_TITLE);
		} else {
			logger.warn("No column '" + TestCaseConstants.CSV_TITLE + "'");
			return caseMapList;
		}
		// Loop through data array read from csv file
		int i = 0;
		while (i < data.length) {
			if (UtilsClass.isNOTNullEmpty(data[i][jTitle])) {
				steps = new ArrayList<>();
				step = new HashMap();
				caseMap = new HashMap();
				// Loop through columns in csv file
				caseMap.put("start_row", i+2);
				for (String csvField : header.keySet()) {
					Object dataField = data[i][header.get(csvField)];
					// Put column data in csv file if the column name is a case field name and data
					// is not null/empty
					if (TestCaseConstants.newCaseMap.keySet().contains(csvField)
							&& UtilsClass.isNOTNullEmpty(dataField)) {
						switch (csvField) {
						case TestCaseConstants.CSV_STEP_CONTENT:
							step.put(TestCaseConstants.TR_STEP_CONTENT, dataField);
							break;
						case TestCaseConstants.CSV_STEP_EXPECTED:
							step.put(TestCaseConstants.TR_STEP_EXPECTED, dataField);
							break;
						case TestCaseConstants.CSV_TYPE:
							caseMap.put(TestCaseConstants.TR_TYPE_ID, (int) TestCaseConstants.typeMap.get(dataField));
							break;
						case TestCaseConstants.CSV_PRIORITY:
							caseMap.put(TestCaseConstants.TR_PRIORITY_ID,(int) TestCaseConstants.priorityMap.get(dataField));
							break;
						case TestCaseConstants.CSV_TEST_LEVEL:
							caseMap.put(TestCaseConstants.TR_TEST_LEVEL,(int) TestCaseConstants.testLevelMap.get(dataField));
							break;
						case TestCaseConstants.CSV_STATUS:
							step.put(TestCaseConstants.TR_STATUS_ID,(int) TestCaseConstants.statusMap.get(dataField));
							break;
						case TestCaseConstants.CSV_IS_AUTOMATED:
							caseMap.put(TestCaseConstants.TR_IS_AUTOMATED,
									TestCaseConstants.isAutomatedMap.get(dataField));
							break;
						default:
							if(UtilsClass.isNumber(dataField.toString())) dataField = (int)Float.parseFloat(dataField.toString()) ;
							caseMap.put(TestCaseConstants.newCaseMap.get(csvField), dataField);
							break;
						}
					}
				}
				if (!step.isEmpty()) {
					steps.add(step);
				}
				i++;
				
				// read next steps while "Title" is still empty
				while (i < data.length) {
					if(UtilsClass.isNullOrEmpty(data[i][jTitle].toString()) || 
								data[i][jTitle].toString().contentEquals(data[i-1][jTitle].toString())) {
						Object stepContent = data[i][header.get(TestCaseConstants.CSV_STEP_CONTENT)].toString().trim();
						Object stepExpected = data[i][header.get(TestCaseConstants.CSV_STEP_EXPECTED)].toString().trim();
						Object stepStatus = data[i][header.get(TestCaseConstants.CSV_STATUS)].toString();
						step = new HashMap();
						if (UtilsClass.isNOTNullEmpty(stepContent)) {
							step.put(TestCaseConstants.TR_STEP_CONTENT, stepContent);
						}
						if (UtilsClass.isNOTNullEmpty(stepExpected)) {
							step.put(TestCaseConstants.TR_STEP_EXPECTED, stepExpected);
						}
						if (UtilsClass.isNOTNullEmpty(stepStatus)) {
							step.put(TestCaseConstants.TR_STATUS_ID, (int) TestCaseConstants.statusMap.get(stepStatus));
						}
	
						if (!step.isEmpty()) {
							steps.add(step);
						}
						i++;
					} else break;
				}
				
				// Put steps in the case
				if (!steps.isEmpty()) {
					caseMap.put(TestCaseConstants.TR_STATUS_ID,getLastStatusFromListStep(steps));
					caseMap.put(TestCaseConstants.TR_STEPS, steps);
				}
				// Add found test case
				caseMap.put("end_row", i+1);
				caseMapList.add(caseMap);
			} else {
				logger.error("Not found test case: Title is empty!");
				i++;
			}
		}
		return caseMapList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addCasesFromCsv(List<HashMap> caseMapList) throws Exception {
		Object sectionName = null;
		String sectionId;
		HashMap<String, String> newSections = new HashMap<>();
		HashMap<String, String> newRuns = new HashMap<>();
		// newSections will contain newly created sections for the list if Section Id is
		// empty
		// A new section will need info of name, project_id, suite_id, and (optional)
		// parent_id for parent section
		for (HashMap caseMap : caseMapList) {
			logger.info("*****Start run from row "+caseMap.get("start_row")+" to row " +caseMap.get("end_row")+"*****");
			logger.info("Processing test case: " + caseMap.get(TestCaseConstants.TR_TITLE));
			Object sId = caseMap.get(TestCaseConstants.TR_SECTION_ID);

			// Check if Section id is empty then create new Section
			if (sId==null) {
				logger.warn("No Section Id, creating new section if necessary...");
				sectionName = caseMap.get(TestCaseConstants.TR_SECTION);
				// System.out.println("sectionName: " + sectionName);

				if (!UtilsClass.isNOTNullEmpty(sectionName)) {
					logger.warn("No Section name, skipped this test case!");
				} else {
					if (newSections.keySet().contains(sectionName)) {
						sectionId = newSections.get(sectionName);
						logger.info("Use created Section: " + sectionId + " (" + sectionName + " ).");
						caseMap.merge(TestCaseConstants.TR_SECTION_ID, sectionId, (valueOld, valueNew) -> valueNew);
					} else {
						// create and save new section to list
						HashMap sectionData = new HashMap();
						sectionData.put(TestCaseConstants.TR_SUITE_ID, caseMap.get(TestCaseConstants.TR_SUITE_ID));
						if (UtilsClass.isNOTNullEmpty(caseMap.get(TestCaseConstants.TR_PARENT_ID))) {
							sectionData.put(TestCaseConstants.TR_PARENT_ID,
									caseMap.get(TestCaseConstants.TR_PARENT_ID));
						}
						sectionData.put(TestCaseConstants.TR_NAME, caseMap.get(TestCaseConstants.TR_SECTION));

						Section sec = new Section();
						sec.setName(sectionName.toString());
						sec.setSuiteId((Integer) caseMap.get(TestCaseConstants.TR_SUITE_ID));
						Section newSec = addSectionByProjectId(caseMap.get(TestCaseConstants.TR_PROJECT_ID).toString(),
								sec);
						sectionId = newSec.getId().toString();
						newSections.put(sectionName.toString(), sectionId);
						logger.info("Newly created Section: " + sectionId + " (" + sectionName + ").");
						caseMap.merge(TestCaseConstants.TR_SECTION_ID, sectionId, (valueOld, valueNew) -> valueNew);
					}
					Case aCase = TestRailUtils.addCaseBySectionId(caseMap.get(TestCaseConstants.TR_SECTION_ID).toString(),
							caseMap);
					caseMap.merge(TestCaseConstants.TR_CASE_ID, aCase.getId(), (valueOld, valueNew) -> valueNew);
					logger.info("Newly created Test Case: " + aCase.getId() + " (" + aCase.getTitle() + ").");
					setRunForCase(caseMap, newRuns);
				}
			} else {
				sectionId = sId.toString();
				// Section Id exists, add case to this section
				logger.info("Add this test case to the Section: " + sectionId);
				Case aCase = TestRailUtils.addCaseBySectionId( caseMap.get(TestCaseConstants.TR_SECTION_ID).toString(),
						caseMap);
				caseMap.merge(TestCaseConstants.TR_CASE_ID, aCase.getId(), (valueOld, valueNew) -> valueNew);
				logger.info("Newly created Test Case: " + aCase.getId() + " (" + aCase.getTitle() + ").");
				setRunForCase(caseMap, newRuns);
			}
			logger.info("*****Run success from row "+caseMap.get("start_row")+" to row " +caseMap.get("end_row")+"*****");
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	  public void setRunForCase(HashMap testCaseMap, HashMap<String, String> newRuns) throws Exception {
			Run aRun;
			Object runId;
			Object runName;
			Object statusID;
			int caseId;
			String caseTitle;
	    runId =  testCaseMap.get(TestCaseConstants.TR_RUN_ID);
	    statusID = testCaseMap.get(TestCaseConstants.TR_STATUS_ID);
			caseId = (int) testCaseMap.get(TestCaseConstants.TR_CASE_ID);
			caseTitle = testCaseMap.get(TestCaseConstants.TR_TITLE).toString();
	    if (UtilsClass.isNOTNullEmpty(runId)) {
	      logger.info("Adding new test case " + caseId + " (" + caseTitle + ") to the Run "
	          + runId + "...");
	      aRun = TestRailUtils.getRunByRunId(runId.toString());
	      if (aRun.getIsIncludeAll()) {
	        {
	          logger.info("The Run '" + aRun.getName()
	              + "' has already included all test cases of test suite '" + aRun.getSuiteId() + "'.");
	        }
	      } else {
	        addCaseToRun(runId.toString(), String.valueOf(caseId));
	        logger.info("Added test case " + caseId + " (" + caseTitle + ") to the Run "
	            + runId + " (" + aRun.getName() + ").");
	      }
	    } else {
	      logger.warn("There is no Run Id found for this test case.");

	      runName = testCaseMap.get(TestCaseConstants.TR_RUN);
	      if (!UtilsClass.isNOTNullEmpty(runName)) {
	        //logger.warn("No Run name, skipped adding this test case to a Run!");
	        logger.error("No Run name found for this test case " + caseId + " (" + caseTitle + ")." );
	      } else if (newRuns.keySet().contains(runName)) {
	        runId = newRuns.get(runName);

	        logger.info("Use created Run: " + runId);
	        addCaseToRun(runId.toString(), String.valueOf(caseId));

	        logger.info("Added test case " + caseId + " (" + caseTitle + ") to the Run "
	            + runId + " (" + runName + ").");
	      } else {
	        // Create new Run and add case_id to the Run

	        logger.info("Creating new Run...");
	        HashMap runData = new HashMap();
	        runData.put(TestCaseConstants.TR_NAME, runName);
	        runData.put(TestCaseConstants.TR_SUITE_ID, testCaseMap.get(TestCaseConstants.TR_SUITE_ID));
	        runData.put(TestCaseConstants.TR_INCLUDE_ALL, false);
	        runData.put(TestCaseConstants.TR_CASE_IDS, new ArrayList<>(Arrays.asList(caseId)));
	        runId = String.valueOf(TestRailUtils
	            .addRunByProjectId(testCaseMap.get(TestCaseConstants.TR_PROJECT_ID).toString(), runData)
	            .getId());

	        newRuns.put(runName.toString(), runId.toString());

	        logger.info("Newly created Run: " + runId + " (" + runName + ").");

	        logger.info("Added test case " + caseId + " to the Run " + runId + " (" + runName + ").");
	      }
	    }
	    Map<String,Object> test = new HashMap<>();
		test.put(TestCaseConstants.TR_RUN_ID, (int)runId);
		test.put(TestCaseConstants.TR_CASE_ID, caseId);
		test.put("custom_step_results", (List<CustomStepsSeparated>) testCaseMap.get(TestCaseConstants.TR_STEPS));
		test.put(TestCaseConstants.TR_STATUS_ID, (int)testCaseMap.get(TestCaseConstants.TR_STATUS_ID));
	    if(UtilsClass.isNOTNullEmpty(statusID))
	      addResultByCaseIdAndRunId(test);
	  }


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addCaseToRun(String runId, String caseId) throws Exception {
		List<Integer> caseIdList = new ArrayList<>();
		core.testrail.entity.Test[] testList;
		testList = TestRailUtils.getTestByRunId(runId);
		for (core.testrail.entity.Test aTest : testList) {
			JSONObject bTest = new JSONObject(new Gson().toJson(aTest));
			caseIdList.add((Integer) bTest.get(TestCaseConstants.TR_CASE_ID));
		}
		// Add case_id to the list of current case_ids
		caseIdList.add(Integer.valueOf(caseId));
		TestRailUtils.updateRunByRunId(runId, new HashMap() {
			private static final long serialVersionUID = -327561456250498125L;
			{
				put(TestCaseConstants.TR_CASE_IDS, caseIdList);
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addCasesToRun(List<HashMap> caseMapList, List<Case> caseList) throws Exception {
		// caseMapList contains test cases read from .csv file (including info of Run *
		// Run Id), caseList contains created test cases
		Run aRun;
		HashMap<String, String> newRuns = new HashMap<>();
		Object runId;
		Object runName;
		Object statusID;
		// core.testrail.entity.Test[] testArray;
		for (Case testCase : caseList) {
			HashMap testCaseMap = new HashMap();
			for (HashMap caseMap : caseMapList) {
				if (UtilsClass.isNOTNullEmpty(caseMap.get(TestCaseConstants.TR_CASE_ID))
						&& caseMap.get(TestCaseConstants.TR_CASE_ID).equals(testCase.getId())) {
					testCaseMap = caseMap;
					break;
				}
			}
			// Add TC to a Run
			runId =  testCaseMap.get(TestCaseConstants.TR_RUN_ID);
			statusID = testCaseMap.get(TestCaseConstants.TR_STATUS_ID);
			if (UtilsClass.isNOTNullEmpty(runId)) {
				logger.info("Adding new test case " + testCase.getId() + " (" + testCase.getTitle() + ") to the Run "
						+ runId + "...");
				aRun = TestRailUtils.getRunByRunId(runId.toString());
				if (aRun.getIsIncludeAll()) {
					{
						logger.info("The Run '" + aRun.getName()
								+ "' has already included all test cases of test suite '" + aRun.getSuiteId() + "'.");
					}
				} else {
					addCaseToRun(runId.toString(), String.valueOf(testCase.getId()));
					logger.info("Added test case " + testCase.getId() + " (" + testCase.getTitle() + ") to the Run "
							+ runId + " (" + aRun.getName() + ").");
				}
			} else {
				logger.warn("There is no Run Id found for this test case.");

				runName = testCaseMap.get(TestCaseConstants.TR_RUN);
				if (!UtilsClass.isNOTNullEmpty(runName)) {
					logger.warn("No Run name, skipped adding this test case to a Run!");
				} else if (newRuns.keySet().contains(runName)) {
					runId = newRuns.get(runName);

					logger.info("Use created Run: " + runId);
					addCaseToRun(runId.toString(), String.valueOf(testCase.getId()));
					
					logger.info("Added test case " + testCase.getId() + " (" + testCase.getTitle() + ") to the Run "
							+ runId + " (" + runName + ").");
				} else {
					// Create new Run and add case_id to the Run

					logger.info("Creating new Run...");
					HashMap runData = new HashMap();
					runData.put(TestCaseConstants.TR_NAME, runName);
					runData.put(TestCaseConstants.TR_SUITE_ID, testCaseMap.get(TestCaseConstants.TR_SUITE_ID));
					runData.put(TestCaseConstants.TR_INCLUDE_ALL, false);
					runData.put(TestCaseConstants.TR_CASE_IDS, new ArrayList<>(Arrays.asList(testCase.getId())));
					runId = String.valueOf(TestRailUtils
							.addRunByProjectId(testCaseMap.get(TestCaseConstants.TR_PROJECT_ID).toString(), runData)
							.getId());
					
					newRuns.put(runName.toString(), runId.toString());
					
					logger.info("Newly created Run: " + runId + " (" + runName + ").");
					
					logger.info("Added test case " + testCase.getId() + " to the Run " + runId + " (" + runName + ").");
				}
			}
			Map<String,Object> test = new HashMap<>();
			test.put(TestCaseConstants.TR_RUN_ID, (int)runId);
			test.put(TestCaseConstants.TR_CASE_ID, testCase.getId());
			test.put("custom_step_results", (List<CustomStepsSeparated>) testCaseMap.get(TestCaseConstants.TR_STEPS));
			test.put(TestCaseConstants.TR_STATUS_ID, (int)testCaseMap.get(TestCaseConstants.TR_STATUS_ID));
			if(UtilsClass.isNOTNullEmpty(statusID))
				addResultByCaseIdAndRunId(test);
		}
	}
	
	public static Result addResultByCaseIdAndRunId(String caseId, String runId, String statusId) throws Exception {
		Result r = null;
		String urlMethod = "add_result_for_case";
		String[] ids = {runId , caseId};
		Map<String,Object> params = new HashMap<>();
		params.put("status_id", statusId);
	    r = (Result) TestRailUtils.runPOSTCustom(urlMethod, ids, params, Result.class);
		return r;
	}
	
	public static Result addResultByCaseIdAndRunId(Map<String, Object> test) throws Exception {
		Result r = null;
		String urlMethod = "add_result_for_case";
		String[] ids = {test.get(TestCaseConstants.TR_RUN_ID).toString() , test.get(TestCaseConstants.TR_CASE_ID).toString()};
		JSONObject testJson = new JSONObject(new Gson().toJson(test));
		//System.out.println(testJson);
	    r = (Result) TestRailUtils.runPOSTCustom(urlMethod, ids, testJson, Result.class);
		return r;
	}
	
	public static Section addSectionByProjectId(String projectId, Section sec) throws Exception {
		Section s = null;
		String urlMethod = "add_section";
		String[] ids = {projectId};
		JSONObject secJson = new JSONObject(new Gson().toJson(sec));
	    s = (Section) TestRailUtils.runPOSTCustom(urlMethod, ids, secJson, Section.class);
		return s;
	}
	
	@SuppressWarnings({"rawtypes" })
	public static int getLastStatusFromListStep(List<HashMap> steps) {
		int status = 0;
		for(int i=0 ; i < steps.size() ; i++) {
			if((int)steps.get(i).get(TestCaseConstants.TR_STATUS_ID) > status) {
				status = (int)steps.get(i).get(TestCaseConstants.TR_STATUS_ID);
			}
		}
		
		return status;
	}
}
