package core;

import java.util.HashMap;

@SuppressWarnings("rawtypes")
public class TestCaseConstants {

	// Constants for ".csv" file of test case list
	public static final String CSV_TITLE = "Title";
	public static final String CSV_REFS = "References";
	public static final String CSV_TYPE = "Type";
	public static final String CSV_PRIORITY = "Priority";
	public static final String CSV_PRECONDS = "Preconditions";
	public static final String CSV_STEP_CONTENT = "Steps (Step)";
	public static final String CSV_STEP_EXPECTED = "Steps (Expected Result)";
	public static final String CSV_TEST_LEVEL = "Test Level";
	public static final String CSV_IS_AUTOMATED = "Is automated yet?";

	public static final String CSV_PROJECT_ID = "Project Id";
	public static final String CSV_SUITE_ID = "Suite Id";
	public static final String CSV_SECTION_ID = "Section Id";
	public static final String CSV_SECTION = "Section";
	public static final String CSV_PARENT_ID = "Parent Id";
	public static final String CSV_RUN_ID = "Run Id";
	public static final String CSV_RUN = "Run";

	public static final String CSV_MILESTONE = "Milestone";
	public static final String CSV_ESTIMATE = "Estimate";
	public static final String CSV_VARIATION = "Variation";
	
	public static final String CSV_STATUS = "Status";

	// Constants for TestRail case field name
	public static final String TR_TITLE = "title";
	public static final String TR_REFS = "refs";
	public static final String TR_TYPE_ID = "type_id";
	public static final String TR_PRIORITY_ID = "priority_id";
	public static final String TR_PRECONDS = "custom_preconds";
	public static final String TR_STEP_CONTENT = "content";
	public static final String TR_STEP_EXPECTED = "expected";
	public static final String TR_TEST_LEVEL = "custom_test_level";
	public static final String TR_IS_AUTOMATED = "custom_is_automated";

	public static final String TR_PROJECT_ID = "project_id";
	public static final String TR_SUITE_ID = "suite_id";
	public static final String TR_SECTION_ID = "section_id";
	public static final String TR_SECTION = "section_name";
	public static final String TR_PARENT_ID = "parent_id";

	public static final String TR_RUN_ID = "run_id";
	public static final String TR_RUN = "run_name";

	public static final String TR_MILESTONE_ID = "milestone_id";
	public static final String TR_ESTIMATE = "estimate";

	public static final String TR_VARIATION = "custom_variation";

	public static final String TR_STEPS = "custom_steps_separated";
	public static final String TR_NAME = "name";
	public static final String TR_CASE_ID = "case_id";
	public static final String TR_CASE_IDS = "case_ids";
	// Run
	public static final String TR_INCLUDE_ALL = "include_all";
	
	public static final String TR_STATUS_ID = "status_id";

	public static HashMap<String, String> newCaseMap = new HashMap<String, String>();
	static {
		newCaseMap.put(CSV_TITLE, TR_TITLE);
		newCaseMap.put(CSV_REFS, TR_REFS);
		newCaseMap.put(CSV_TYPE, TR_TYPE_ID);

		newCaseMap.put(CSV_PRIORITY, TR_PRIORITY_ID);
		newCaseMap.put(CSV_PRECONDS, TR_PRECONDS);
		newCaseMap.put(CSV_STEP_CONTENT, TR_STEP_CONTENT);
		newCaseMap.put(CSV_STEP_EXPECTED, TR_STEP_EXPECTED);

		newCaseMap.put(CSV_TEST_LEVEL, TR_TEST_LEVEL);
		newCaseMap.put(CSV_IS_AUTOMATED, TR_IS_AUTOMATED);

		newCaseMap.put(CSV_PROJECT_ID, TR_PROJECT_ID);
		newCaseMap.put(CSV_SUITE_ID, TR_SUITE_ID);
		newCaseMap.put(CSV_SECTION_ID, TR_SECTION_ID);
		newCaseMap.put(CSV_SECTION, TR_SECTION);
		newCaseMap.put(CSV_PARENT_ID, TR_PARENT_ID);

		newCaseMap.put(CSV_RUN_ID, TR_RUN_ID);
		newCaseMap.put(CSV_RUN, TR_RUN);
		
		newCaseMap.put(CSV_STATUS, TR_STATUS_ID);
		// newCaseMap.put(CSV_MILESTONE, TR_MILESTONE_ID);
		// newCaseMap.put(CSV_ESTIMATE, TR_ESTIMATE);
		// newCaseMap.put(CSV_VARIATION, TR_VARIATION);
	}

	public static HashMap<String, String> newSectionMap = new HashMap<String, String>();
	static {
		newSectionMap.put(CSV_PROJECT_ID, TR_PROJECT_ID);
		newSectionMap.put(CSV_SUITE_ID, TR_SUITE_ID);
		newSectionMap.put(CSV_PARENT_ID, TR_PARENT_ID);
		newSectionMap.put(CSV_SECTION, TR_SECTION);
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, Integer> typeMap = new HashMap();

	static {
		typeMap.put("API", 15);
		typeMap.put("Boundary & Validation", 14);
		typeMap.put("Integration", 16);
		typeMap.put("Negative", 5);
		typeMap.put("Other", 7);
		typeMap.put("Performance", 8);
		typeMap.put("Positive", 6);
		typeMap.put("Security", 10);
		typeMap.put("Smoke", 11);
		typeMap.put("UI", 13);
		typeMap.put("Usability", 12);
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, Integer> priorityMap = new HashMap();

	static {
		priorityMap.put("Critical", 4);
		priorityMap.put("High", 3);
		priorityMap.put("Medium", 2);
		priorityMap.put("Low", 1);
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, Integer> testLevelMap = new HashMap();

	static {
		testLevelMap.put("Unit", 1);
		testLevelMap.put("Component/API", 2);
		testLevelMap.put("Integration", 3);
		testLevelMap.put("SIT", 4);
		testLevelMap.put("UAT", 5);
		testLevelMap.put("UI", 6);
		testLevelMap.put("Contract", 7);
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, Boolean> isAutomatedMap = new HashMap();

	static {
		isAutomatedMap.put("Yes", true);
		isAutomatedMap.put("No", false);
	}

	@SuppressWarnings("unchecked")
	public static HashMap<String, Integer> templateMap = new HashMap();

	static {
		templateMap.put("Exploratory Session", 3);
		templateMap.put("Test Case (Steps)", 2);
		templateMap.put("Test Case (Text)", 1);
	}
	
	@SuppressWarnings("unchecked")
	public static HashMap<String, Integer> statusMap = new HashMap();

	static {
		statusMap.put("Passed", 1);
		statusMap.put("Blocked", 2);
		statusMap.put("Untested", 3);
		statusMap.put("Retest", 4);
		statusMap.put("Failed", 5);
		statusMap.put("Deferred", 6);
		statusMap.put("pending", 7);
	}
}
